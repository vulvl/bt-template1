/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  ImageBackground,
  TextInput,
  CheckBox
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Screen1 from './screens/screen1'
import Screen2 from './screens/screen2'
import Screen3 from './screens/screen3'
import Menu from './screens/menu'

const App: () => React$Node = () => {
  const imageSrc = 'https://r1.ilikewallpaper.net/iphone-8-wallpapers/download/35311/Night-Sky-Sunset-Pink-Nature-iphone-8-wallpaper-ilikewallpaper_com.jpg'
  return (
    <>
      <ImageBackground
        source={{uri: imageSrc}}
        style={styles.image}
      >
        <Screen1 />
      </ImageBackground>
    </>
  );
};

const styles = StyleSheet.create({
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
    alignItems: 'center'
  },
})

export default App;
