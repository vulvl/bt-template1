import React from 'react'
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import IconEntypo from 'react-native-vector-icons/Entypo';
import Menu from './menu'

const Screen2 = () => {
  return (
    <View style={styles.container}>
      <View style={styles.formSearchBar}>
        <TextInput style={styles.searchbar}></TextInput>
        <Icon name="search1" style={styles.iconSearch}/>
      </View>
      <View style={styles.frmInfo}>
        <View>
          <Image 
            style={styles.avatar}
            source={{
              uri: 'https://66.media.tumblr.com/b48e49072d459302097e78ab9452c9fc/cf601363ed97e4af-aa/s1280x1920/a7ed1c36249234b9909ddcabd0616f3579928b31.png',
            }}
          />
        </View>
        <View style={styles.infoUser}>
          <Text style={styles.name}>Mina Myoui</Text>
          <Text style={styles.number}>Booking: 247LON18</Text>
        </View>
        <View style={styles.lstBtn}>
          <TouchableOpacity style={styles.btn}>
            <Text style={styles.txtInfo}>Current Booking</Text>
            <IconEntypo name="chevron-right" style={styles.iconRight}></IconEntypo>
          </TouchableOpacity>
          <TouchableOpacity style={styles.btn}>
            <Text style={styles.txtInfo}>Previous Bookings</Text>
            <IconEntypo name="chevron-right" style={styles.iconRight}></IconEntypo>
          </TouchableOpacity >
          <TouchableOpacity style={styles.btn}>
            <Text style={styles.txtInfo}>My Profile</Text>
            <IconEntypo name="chevron-right" style={styles.iconRight}></IconEntypo>
          </TouchableOpacity>
          <TouchableOpacity style={styles.btn}>
            <Text style={styles.txtInfo}>Urgent Assistance</Text>
            <IconEntypo name="chevron-right" style={styles.iconRight}></IconEntypo>
          </TouchableOpacity>
        </View>
      </View>
      <Menu name='menu'/>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center'
  },
  formSearchBar: {
    flexDirection: 'row',
    backgroundColor: 'white',
    width: 380,
    height: 40,
    marginTop: 20,
    borderRadius: 35,
    alignItems: 'center'
  },
  searchbar: {
    width: 300,
    height: 40,
    marginLeft: 20
  },
  iconSearch: {
    marginLeft: 15,
    fontSize: 25,
    color: 'gray'
  },
  frmInfo: {
    backgroundColor: '#fff',
    width: 380,
    height: 400,
    borderRadius: 25,
    shadowRadius: 16,
    alignItems: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 7,
    marginTop: 100
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 80,
    borderWidth: 4,
    borderColor: '#fff',
    marginTop: -64
  },
  infoUser: {
    marginTop: 20,
    alignItems: 'center'
  },
  name: {
    fontFamily: 'Sarpanch-Medium',
    fontSize: 20
  },
  number: {
    fontSize: 12,
    color: 'gray',
    fontFamily: 'Sarpanch-Medium',
  },
  lstMenu: {
    backgroundColor: 'black',
    height: 20,
  },
  lstBtn: {
    marginTop: 25,
  },
  btn: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#E8E8E8',
    width: 320,
    height: 50,
    marginBottom: 8,
    borderRadius: 50,
    paddingLeft: 20,
    paddingRight: 20
  },
  txtInfo: {
    fontFamily: 'Sarpanch-Medium'
  },
  iconRight: {
    fontSize: 25,
    fontWeight: 'bold'
  }
})

export default Screen2