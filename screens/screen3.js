import React from 'react'
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  FlatList
} from 'react-native';
import Menu from './menu'
import IconIonicons from 'react-native-vector-icons/Ionicons'
import IconEntypo from 'react-native-vector-icons/Entypo'
import IconFeather from 'react-native-vector-icons/Feather'

const data = [
  { key: 'Home'},
  { key: 'Live Flight Details'},
  { key: 'Checked Baggage'},
  { key: 'Hand Baggage'},
  { key: 'Essential Information'},
  { key: 'Booking Conditions'},
  { key: 'Amendment & Cancellation'},
  { key: 'Airport Hotel & Parking'},
  { key: 'ATOL Certificate'},
  { key: 'Visa & Passport'},
  { key: 'Insurance Policy'},
  { key: 'Manage My Booking'},
]

const Screen3 = () => {
  return (
    <View  style={styles.container}>
      <View style={styles.frmList}>
        <View style={styles.list}>
          <FlatList
            data={data}
            renderItem={({item}) => <Text style={styles.item}>{item.key}</Text>}
          />
        </View>
        <View style={styles.listBtn}>
          <View style={styles.power}>
            <TouchableOpacity style={styles.btnPower}>
              <IconIonicons name="md-power" style={styles.iconPower}/>
            </TouchableOpacity>
          </View>
          <View style={styles.btnsOption}>
            <TouchableOpacity style={styles.btnOption}>
              <IconEntypo name="home" style={styles.iconOption}/>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btnOption}>
              <IconIonicons name="md-settings" style={styles.iconOption}/>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btnOption}>
              <IconFeather name="more-horizontal" style={styles.iconOption}/>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btnOption}>
              <IconEntypo name="tv" style={styles.iconOption}/>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <Menu name='close' />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center'
  },
  frmList: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    width: 380,
    height: 540,
    borderRadius: 25,
    shadowRadius: 16,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 7,
    marginTop: 20
  },
  item: {
    borderBottomWidth: 0.6,
    borderBottomColor: "#B5B5B5",
    marginLeft: 30,
    marginTop: 20,
    paddingBottom: 10,
    fontFamily: 'Sarpanch-Medium'
  },
  list: {
    width: 270,
    marginTop: 20
  },
  listBtn: {
    flex: 1,
    flexDirection: 'column'
  },
  power: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 30
  },
  btnsOption: {
    flex: 2,
    alignItems: 'center',
    marginTop: 120
  },
  btnPower: {
    backgroundColor: '#ECECEC',
    width: 50,
    height: 50,
    borderRadius: 50,
    alignItems: 'center',
    paddingTop: 12,
  },
  iconPower: {
    fontSize: 25,
  },
  iconOption: {
    fontSize: 25,
  },
  btnOption: {
    backgroundColor: '#ECECEC',
    width: 50,
    height: 50,
    borderRadius: 50,
    alignItems: 'center',
    paddingTop: 12,
    marginBottom: 10
  }
})

export default Screen3