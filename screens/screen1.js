import React from 'react'
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import CheckBox from '@react-native-community/checkbox';

const Screen1 = () => {
  const [name, onChangeName] = React.useState('Passenger Full Name')
  const [number, onChangeNumber] = React.useState('Booking Number')
  return (
    <View style={styles.container}>
      <View style={styles.fmLogin}>
        <TextInput 
          value={name} 
          style={styles.input}
          onChangeText={text => onChangeName(text)}
        ></TextInput>
        <TextInput 
          value={number} 
          style={styles.input}
          onChangeText={text => onChangeNumber(text)}
        ></TextInput>
        <View style={styles.remMe}>
          <Text style={{fontFamily: 'Sarpanch-Medium'}}>Remember me</Text>
          <CheckBox
            value={false}
            style={{borderColor: 'gray'}}
          />
        </View>
        <TouchableOpacity>
          <LinearGradient colors={['#f74e7f', '#f74e7f', '#f87b48']} start= {{ x: -1, y: 0 }}end= {{ x: 1, y: 0 }} style={styles.btnLogin}>
              <Text style={styles.txtLogin}>Login</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: 'center'
  },
  fmLogin: {
    backgroundColor: '#fff',
    width: 380,
    height: 280,
    borderRadius: 25,
    shadowRadius: 16,
    alignItems: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 7,
  },
  input: {
    borderWidth: 1,
    width: 310,
    height: 60,
    marginTop: 20,
    borderRadius: 35,
    borderColor: '#DCDCDC',
    paddingLeft: 30,
    fontFamily: 'Sarpanch-Medium'
  },
  remMe: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'flex-end',
    marginTop: 30,
    marginRight: 30,
  },
  btnLogin: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: 220,
    borderRadius: 35,
    marginTop: 27,
    marginBottom: 195,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 7,
  },
  txtLogin: {
    color: 'white',
    fontFamily: 'Sarpanch-Medium',
    fontSize: 18,
  }
});

export default Screen1;
