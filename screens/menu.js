import React from 'react'
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import IconCommu from 'react-native-vector-icons/MaterialCommunityIcons'
import IconEntypo from 'react-native-vector-icons/Entypo'
import IconMaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import LinearGradient from 'react-native-linear-gradient';

const Menu = (props) => {
  const iconMenu = 
    props.name == 'menu' ? 
      <IconEntypo name='menu' style={styles.iconMenu}/> : 
      <IconMaterialCommunityIcons name='close' style={styles.iconMenu}/>
  return (
    <View style={styles.container}>
      <View style={styles.menu}>
        <View style={styles.item}>
          <Icon name="bars" style={styles.icon}/>
          <Text style={styles.txtMenu}>ITNERARY</Text>
        </View>
        <View style={styles.item}>
          <Icon name="user" style={styles.icon}/>
          <Text style={styles.txtMenu}>ACCOUNT</Text>
        </View>
        <View >
          <TouchableOpacity>
            <LinearGradient 
              colors={['#f74e7f', '#f74e7f', '#f87b48']} 
              start= {{ x: -1, y: 0 }}end= {{ x: 1, y: 0 }} 
              style={styles.btnMenu}
            >
              <View>
                {iconMenu}
              </View>
            </LinearGradient>
          </TouchableOpacity>
        </View>
        <View style={styles.item}>
          <Icon name="bells" style={[styles.icon]}/>
          <Text style={styles.txtMenu}>NOTIFICATIONS</Text>
        </View>
        <View style={styles.item}>
          <IconCommu name="comment-text-outline" style={styles.icon}/>
          <Text style={styles.txtMenu}>CONTACT</Text>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  menu: {
    backgroundColor: '#181818',
    width: 420,
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  icon: {
    fontSize: 25,
    color: 'white',
    marginTop: 10
  },
  txtMenu: {
    color: '#fff',
    fontSize: 9,
    marginTop: 6
  },
  item: {
    alignItems: 'center',
  },
  btnMenu: {
    width: 70,
    height: 70,
    borderRadius: 50,
    marginTop: -25,
    alignItems: 'center',
    marginRight: -35,
    marginLeft: -25
  },
  iconMenu: {
    fontSize: 40,
    color: 'white',
    marginTop: 5,
    paddingTop: 10
  }
})

export default Menu